# KnabTest

Behavior Driven Development Cucumber - Selenium based automation framework including report.

Improvements ==>
1. Add more tests for moving Tasks 
2. Deleting tasks
3. reporting some fancy reports with graphs
4. Update README file

This framework contains code containing:

One feature (feature file)
Three Scenarios

Scenario 1 To validate the Board creation functionality of the Trello Application
Scenario 2 Check Creating List , Cards and moving task between list functionality
Scenario 3 API call functionality for Board , List and Card



## Steps to run at your system:

Clone the repository using "git clone" 
Right click on feature file & Select Run.
Or  Run "mvn clean test"

Or integrate with CI tools Jenkins

## Reports:
Added json & HTML report -> (To DO add more interactive reports using plugins)
"json:target/cucumber.json",
 "html:target/HTMLReport.html"


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/jaihindpatil/knabtest/-/settings/integrations)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

