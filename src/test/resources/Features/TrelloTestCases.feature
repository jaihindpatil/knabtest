@SanityTest
Feature: Verification of UI and Rest API tests for Trello


  Scenario: To validate the Board creation functionality of the Trello Application
    Given Launch the Application
    When Enter the valid username and password
    And Login should be successfull

  Scenario: Check Creating List , Cards and moving task between list functionality
    Given Launch the Application
    When Enter the valid username and password
    And Login should be successfull
    When Create new board and Enter Title
    Then Validate the cards addition functionality for the list named "Test Task"
      | Task1 |
      | Task2 |
      | Task3 |
    Then Editing the task on the list
      | Edited Task | Description:add description |

  Scenario: API call functionality for Board , List and Card
    Given Create a board through API call
    And Get the board name through API call
    Then Create a list for the board through API call as "My Task"
    And Create a card for the list through API call as "Test case creation"
    Then Delete a board through API call
