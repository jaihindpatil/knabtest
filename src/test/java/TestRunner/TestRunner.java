package TestRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/Features",
        glue = { "stepDefinations" },
       plugin = {"pretty", "junit:target/JUnitReports/report.xml",
                "json:target/cucumber.json",
                "html:target/HTMLReport.html",
        },
        tags = "@SanityTest" )

public class TestRunner {


}
